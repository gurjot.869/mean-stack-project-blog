import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { from } from 'rxjs';
import { RegisterService } from '../service/register/register.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  registerForm:FormGroup
  constructor(private fb:FormBuilder, private register:RegisterService,private router:Router) { }

  ngOnInit(): void {
    this.registerForm=this.fb.group({
      YourFirstName:[''],
      YourLastName:[''],
      YourUsername:[''],
       email:[''],
       password:[''],
      Retypethepassword:[''],
      City:[''],
      State:[''],
      Zip:['']
    })

  }

  async Register(){
    const res:any = await this.register.Register(this.registerForm.value)
    if(res.success){
      alert(res.message)
    }
    else
    alert(res.message)
  }


}
