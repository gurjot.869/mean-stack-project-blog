import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { LoginService } from '../service/login/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  constructor(private login:LoginService, private fb:FormBuilder, private router:Router) { }

  ngOnInit(): void {
    this.loginForm=this.fb.group({
      email:[''],
      password:['']
    })
  }

  async Login(){
    const res:any = await this.login.login(this.loginForm.value)
    await this.login.setJwt(res.data);
    // console.log('1236');
    this.router.navigateByUrl('/blogpost')
  

  }

}
