import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BlogpostService } from '../service/blogpost/blogpost.service';
import { LoginService } from '../service/login/login.service';
import { CategoryService } from '../service/category/category.service';
import { SubcategoryService } from '../service/subcategory/subcategory.service';

@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.css']
})
export class BlogpostComponent implements OnInit {

  constructor(private fb:FormBuilder ,private category: CategoryService, private Subcategory: SubcategoryService, private router:Router,private blog:BlogpostService , private login:LoginService) { }

  blogForm:FormGroup;
  allCategoryObj;
  allSubCategoryObj;
  tempAllSubCategoryObj;
  tempImageArr: any = [];
  displayArr: any = [];
  ngOnInit(): void {
    this.blogForm=this.fb.group({
      
      Title:[''],
      category:[''],
      blogContent:[''],
      subcategory: [''],
      imageArr: []
    })
    this.getCategorys();
    this.getSubcategorys();

    // console.log(this.blogForm.value)
  }

  async getCategorys() {
    const res: any = await this.category.getCategorys();
    this.allCategoryObj = res.data;
  }

  async addBlog() {
    const res: any = await this.blog.addBlog(this.blogForm.value);
    if (res.success) {
      alert(res.message)
    }
    else {
      alert(res.message)
    }
    window.location.reload();
  }

  async getSubcategorys() {
    const res: any = await this.Subcategory.getSubcategorys();
    this.allSubCategoryObj = res.data;
    this.tempAllSubCategoryObj = res.data;

  }


  uploadImage(event) {

    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.tempImageArr.push(reader.result);
        this.displayArr.push(event.target.value.split(/(\\|\/)/g).pop());
        this.blogForm.patchValue({
          imageArr: this.tempImageArr
        })
        // console.log(this.specificationForm.value);
      }
    }

  }
  //////////////////////////////deleting  image from array
  deleteImage(i) {
    this.tempImageArr.splice(i, 1)
    this.displayArr.splice(i, 1)
    this.blogForm.patchValue({
      imageArr: this.tempImageArr
    })
  }

  /////////////////////filter logic starts here 

  filterSubCategory(event) {
    let tempId = event.target.value;
    this.tempAllSubCategoryObj = this.allSubCategoryObj.filter(el => el.categoryId == tempId)
  

}

editorConfig: AngularEditorConfig = {
  editable: true,
  spellcheck: true,
  height: 'auto',
  minHeight: '500px',
  maxHeight: '700px',
  width: '500px',
  minWidth: '0',
  translate: 'yes',
  enableToolbar: true,
  showToolbar: true,
  placeholder: 'Enter text here...',
  defaultParagraphSeparator: '',
  defaultFontName: '',
  defaultFontSize: '',
  fonts: [
    { class: 'arial', name: 'Arial' },
    { class: 'times-new-roman', name: 'Times New Roman' },
    { class: 'calibri', name: 'Calibri' },
    { class: 'comic-sans-ms', name: 'Comic Sans MS' }
  ],
  customClasses: [
    {
      name: 'quote',
      class: 'quote',
    },
    {
      name: 'redText',
      class: 'redText'
    },
    {
      name: 'titleText',
      class: 'titleText',
      tag: 'h1',
    },
  ],
  uploadUrl: 'v1/image',
  uploadWithCredentials: false,
  sanitize: true,
  toolbarPosition: 'top',
  toolbarHiddenButtons: [
    ['bold', 'italic'],
    ['fontSize']
  ]
};



}
