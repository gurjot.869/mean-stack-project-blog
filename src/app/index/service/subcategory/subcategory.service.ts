import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {

  url = 'http://localhost:3000';
  constructor(private http: HttpClient) { }

  async addSubcategory(formData) {
    return this.http.post(`${this.url}/subcategory/addSubcategory`, formData).toPromise()
  }

  async getSubcategorys() {
    return this.http.get(`${this.url}/subcategory/getAllSubcategorys`).toPromise();
  }

  async deleteSubcategory(id) {
    return this.http.delete(`${this.url}/subcategory/deleteSpecificSubcategory/${id}`).toPromise();
  }

  async updateSubcategory(formData, id) {
    return this.http.patch(`${this.url}/subcategory/updateSpecificSubcategory/${id}`, formData).toPromise()
  }

  async getParticularSubcategory(id) {
    return this.http.get(`${this.url}/subcategory/getSpecificSubcategory/${id}`).toPromise()
  }
}
