import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { AdminloginService } from '../adminservices/adminlogin/adminlogin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  loginForm: FormGroup
  constructor(private login:AdminloginService, private fb:FormBuilder, private router:Router) { }

  ngOnInit(): void {
    this.loginForm=this.fb.group({
      email:[''],
      password:['']
  })
}



async Login(){
  const res:any = await this.login.login(this.loginForm.value)
  await this.login.setJwt(res.data);
  // console.log('1236');
  this.router.navigateByUrl('/admin')

}
}
