import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user/user.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private user:UserService, private fb:FormBuilder, private route:ActivatedRoute) { }
  userArr;
  modalId;

  registerForm:FormGroup
  ngOnInit(): void {
    this.registerForm=this.fb.group({
      email:[''],
      password:['']
    })
    this.getUser();
  }

  setModalId(id){
    this.modalId=id; 
  }



  async updateUser(){
    const res:any= await this.user.updateUser(this.modalId,this.registerForm.value)
    if(res.success) {
      alert(res.message)
    }
    else{
      alert(res.message)
    }
    window.location.reload();
  }


  async getUser(){
    const res:any = await this.user.getUser();
    if(res.success) {
      this.userArr=res.data;
      console.log(this.userArr)
    }
    else{
      alert(res.message)
    }
  }



  async deleteUser(id){
    const res:any = await this.user.deleteUser(id)
    if(res.success) {
      alert(res.message)
    }
    else{
      alert(res.message)
    }
    this.ngOnInit();
  }

}

  


