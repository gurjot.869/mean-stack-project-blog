import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './index/header/header.component';
import { BodyComponent } from './index/body/body.component';
import { FooterComponent } from './index/footer/footer.component';
import { MyblogComponent } from './index/myblog/myblog.component';
import { MystoryComponent } from './index/mystory/mystory.component';
import { ContactComponent } from './index/contact/contact.component';
import { BlogpostComponent } from './index/blogpost/blogpost.component';
import { RegisterComponent } from './index/register/register.component';
import { LoginComponent } from './index/login/login.component';
import { AdminComponent } from './admin-index/admin/admin.component';
import { AdminLoginComponent } from './admin-index/admin-login/admin-login.component';
import { AdminRegisterComponent } from './admin-index/admin-register/admin-register.component';
import { UserdashboardComponent } from './user/userdashboard/userdashboard.component';
import { UserheaderComponent } from './user/userheader/userheader.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminAddCategoryComponent } from './admin-add-category/admin-add-category.component';
import { AdminAddSubCategoryComponent } from './admin-add-sub-category/admin-add-sub-category.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    MyblogComponent,
    MystoryComponent,
    ContactComponent,
    BlogpostComponent,
    RegisterComponent,
    LoginComponent,
    AdminComponent,
    AdminLoginComponent,
    AdminRegisterComponent,
    UserdashboardComponent,
    UserheaderComponent,
    AdminAddCategoryComponent,
    AdminAddSubCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularEditorModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
