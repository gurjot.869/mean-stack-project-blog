var mongoose = require('mongoose');
var register = mongoose.Schema({
    Firstname:String,
    Lastname:String,
    Email:String,
    Password:String,
    Repassword:String,
    City:String,
    State:String,
    Zip:String
})

module.exports = mongoose.model('Register',register);