let bcrypt = require('bcryptjs')

const encryptPassword = async (hashedPassword) => {

    return new Promise((resolve,reject)=>{

        bcrypt.hash(hashedPassword, 10, (err, hash)=> {
            if(err){
                reject(err)
            }
            else{
                resolve(hash)
            }
        });
    })
}


module.exports = {
    encryptPassword
}